function CentreOnPage() {
    var containerWidth = $(".row").width();
    var myWidth = $("#circle").width();
    var leftMargin = Math.floor(containerWidth / 2 - (myWidth / 2));
    $("#circle").css("marginLeft", leftMargin);
    $("#circle").css("marginTop", -Number(Math.floor(($(document).height()/100)*5)));
}

$("#misc").click(function() {
    $("html, body").animate({ scrollTop: $('.scrolltarget').offset().top }, 2000);
});

$(window).ready(function() {
    CentreOnPage();
    $(window).resize(function() {
        CentreOnPage();
    });
});