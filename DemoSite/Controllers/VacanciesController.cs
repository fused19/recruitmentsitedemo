﻿using System;
using System.Collections.Generic;
using DemoSite.Models;
using System.Web.Mvc;

namespace DemoSite.Controllers
{
    public class VacanciesController : Controller
    {
        [HttpGet]
        public ViewResult Index()
        {
            /*
            * Assign page title with view bag
            * Make a AdModel list and pass to view
            */
            ViewBag.title = "Vacancies";
            List<Models.AdModel> modelList = Domain.ContentCreation.CreationUtilties.MakeJobAd();
            return View(modelList);
        }

        [HttpGet]
        public ViewResult Vacancy(AdModel ad)
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Job";
            return View(ad);
        }

        [HttpGet]
        public ActionResult FindOutMore(string Title, string Author, string Description, string EndAd, string startAd)
        {
            /*
            * Assign page title with view bag
            * Create a model using variables passed to pass model to view
            */
            ViewBag.title = "Find out more";
            var model = new AdModel(){Title= Title, Author = Author, Description = Description, EndAd = new DateTime(), startAd = new DateTime()};
            return RedirectToAction("Vacancy", model);
        }
    }
}