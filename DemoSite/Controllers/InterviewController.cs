﻿using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using DemoSite.Models;

namespace DemoSite.Controllers
{
    public class InterviewController : Controller
    {
        [HttpGet]
        public ViewResult Index()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Interview request";
            return View();
        }

        [HttpGet]
        public ViewResult InterviewForm()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Interview request";
            return View();
        }

        [HttpPost]
        public ActionResult InterviewForm(VacancyApplyModel vacancy)
        {
            /*
            * Assign page title with view bag
            * Check model is correct or act accordingly
            */
            ViewBag.title = "Interview request";
            if (ModelState.IsValid)
            {
                ViewBag.title = "Interview form";
                return View("ThankYou", vacancy);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ViewResult ThankYou()
        {
            /*
            * Assign page title with view bag
            */
            ViewBag.title = "Thankyou";
            return View();
        }
    }
}