﻿using System.Collections.Generic;
using DemoSite.Models;
using System.Web.Mvc;

namespace DemoSite.Controllers
{
    public class BlogController : Controller
    {
        [HttpGet]
        public ViewResult Index()
        {
            /*
            * Assign page title with view bag, create a model and pass to view
            */
            ViewBag.title = "Blog";
            List<BlogModel> modelList = Domain.ContentCreation.CreationUtilties.MakeBlogPosts();
            return View(modelList);
        }
    }
}