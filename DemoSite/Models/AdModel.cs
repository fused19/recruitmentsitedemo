﻿using System;

namespace DemoSite.Models
{
    public class AdModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime startAd { get; set; }

        public DateTime EndAd { get; set; }

        public string Author { get; set; }
    }
}