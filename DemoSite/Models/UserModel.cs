﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DemoSite.Models
{
    public class UserModel
    {
        [DisplayName("Email:")]
        [Required(ErrorMessage="A valid email address are required")]
        [RegularExpression(".+\\@.+\\..+",ErrorMessage="All email address have at @ symbol")]
        public string Email { get; set; }
        [DisplayName("Password:")]
        [Required(ErrorMessage="A password is needed.")]
        public string Password { get; set; }
    }
}