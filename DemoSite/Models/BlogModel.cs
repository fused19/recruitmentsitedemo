﻿using System;

namespace DemoSite.Models
{
    public class BlogModel
    {
        public string Title { get; set; }

        public string ByLine { get; set; }

        public string BodyCopy { get; set; }

        public DateTime PublishedDate { get; set; }

        public string Author { get; set; }
    }
}